import os

ALLOW_URL_CORS = [
    "https://localhost:10888",
    "http://localhost:8080",
]
VERIFY_SSL = False
VK_API_ACCESS_TOKEN = os.getenv('VK_API_ACCESS_TOKEN')
APP_API = os.getenv('APP_API')
TOKEN_TELEGRAM = os.getenv("TOKEN_TELEGRAM")
TOKEN_VIBER = os.getenv("TOKEN_VIBER")
