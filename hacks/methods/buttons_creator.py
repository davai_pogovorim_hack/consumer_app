from telebot.types import KeyboardButton, ReplyKeyboardMarkup


class CreateButtons:

    def __init__(self, array_texts):
        self.array = [KeyboardButton(a) for a in array_texts]

    def create_buttons(self):
        btns = self.create()
        if btns is None:
            return None
        keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        for row in btns:
            keyboard.row(*row)
        return keyboard

    def create_buttons_1(self):
        return [[self.array[0]]]

    def create_buttons_2(self):
        return [[self.array[0], self.array[1]]]

    def create_buttons_3(self):
        return [
            [self.array[0], self.array[1]],
            [self.array[2]]
        ]

    def create_buttons_4(self):
        return [
            [self.array[0], self.array[1]],
            [self.array[2], self.array[3]]
        ]

    def create_buttons_5(self):
        return [
            [self.array[0], self.array[1]],
            [self.array[2], self.array[3]],
            [self.array[4]]
        ]

    def create_buttons_6(self):
        return [
            [self.array[0], self.array[1]],
            [self.array[2], self.array[3]],
            [self.array[4], self.array[5]],
        ]

    def create_buttons_7(self):
        return [
            [self.array[0], self.array[1], self.array[2]],
            [self.array[3], self.array[4], self.array[5]],
            [self.array[6]],
        ]

    def create_buttons_8(self):
        return [
            [self.array[0], self.array[1], self.array[2]],
            [self.array[3], self.array[4], self.array[5]],
            [self.array[6], self.array[7]],
        ]

    def create_buttons_9(self):
        return [
            [self.array[0], self.array[1], self.array[2]],
            [self.array[3], self.array[4], self.array[5]],
            [self.array[6], self.array[7], self.array[8]],
        ]

    def create(self):
        b = [
            None,
            self.create_buttons_1,
            self.create_buttons_2,
            self.create_buttons_3,
            self.create_buttons_4,
            self.create_buttons_5,
            self.create_buttons_6,
            self.create_buttons_7,
            self.create_buttons_8,
            self.create_buttons_9,
        ]
        try:
            return b[len(self.array)]()
        except IndexError:
            return None


def create_keyboard_by_texts(array_texts):
    return CreateButtons(array_texts).create_buttons()


