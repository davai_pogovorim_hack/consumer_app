import asyncio
from fastapi_jsonrpc import BackgroundTasks
from viberbot.api.messages import TextMessage
from viberbot import Api as api_viber, BotConfiguration

from hacks.app import api_v1
from hacks.methods.buttons_creator_viber import create_keyboard_by_texts_viber
from hacks.validates.service_ans import ResponseApp
import uuid
from hacks.settings import conf
import telebot
from hacks.methods.buttons_creator import create_keyboard_by_texts
bot = telebot.TeleBot(conf.TOKEN_TELEGRAM)
viber = api_viber(BotConfiguration(
    name='Давай поговорим',
    avatar='https://psv4.userapi.com/c856228/u243615417/docs/d15/04a5e0dde1d3/avatarka.jpg?extra=-uS7aMsHbliC66YYNfAcBSucjcc7YzdlekndsaB0uWSfVF0iGc1qQ9a4k2BHXfG9QuErYGpbipzlSMerOOR_IA4Qwa-Y34JUQXVH7fZgDSliyxJ5GdN7MUfLv_14VbHMaZwaOheDXyBbSo2YNyyBInuZGw',
    auth_token=conf.TOKEN_VIBER
))


@api_v1.method()
async def echo() -> dict:
    return dict(
        status="ok",
        text="api is start"
    )


@api_v1.method()
async def send_to_vk(
        tasks: BackgroundTasks,
        data: ResponseApp,
) -> dict:
    def logic(data: ResponseApp):
        import vk_api
        vk_session = vk_api.VkApi(token=conf.VK_API_ACCESS_TOKEN)
        vk = vk_session.get_api()
        vk.messages.send(
            peer_id=data.user_id,
            message=data.text,
            keyboard=data.get_keyboards_vk(),
            random_id=uuid.uuid4().int & (1 << 32) - 1
        )
    tasks.add_task(
        func=logic,
        data=data,
    )
    return dict(status="ok")

@api_v1.method()
async def send_to_telegram(
        tasks: BackgroundTasks,
        data: ResponseApp,
) -> dict:
    def logic(data: ResponseApp):
        import textwrap
        for texts in list(textwrap.wrap(data.text, width=1000)):
            params = dict(
                chat_id=data.user_id,
                text=texts,
            )
            keyboard = data.get_keyboards_telegram(create_keyboard_by_texts)
            if keyboard is not None:
                params["reply_markup"] = keyboard
            bot.send_message(**params)
    tasks.add_task(
        func=logic,
        data=data,
    )
    return dict(status="ok")


@api_v1.method()
async def send_to_viber(
        tasks: BackgroundTasks,
        data: ResponseApp,
) -> dict:
    def logic(data: ResponseApp):
        import textwrap
        for texts in list(textwrap.wrap(data.text, width=1000)):
            params = TextMessage(text=texts)
            keyboard = data.get_keyboards_telegram(create_keyboard_by_texts_viber)
            viber.send_messages(data.user_id, [params])
            if keyboard is not None:
                viber.send_messages(data.user_id, [keyboard])

    tasks.add_task(
        func=logic,
        data=data,
    )
    return dict(status="ok")

