from viberbot.api.messages import KeyboardMessage


class CreateButtons:
    def __init__(self, array_texts):
        self.array = array_texts

    @staticmethod
    def create_key(text, col=6, row=1):
        return {
            "Columns": col,
            "Rows": row,
            "BgLoop": False,
            "ActionType": "reply",
            "ActionBody": text,
            "ReplyType": "message",
            "Text": text,
        }

    def create_buttons(self):
        proxy = [self.create_buttons_1,
                 self.create_buttons_2,
                 self.create_buttons_3]
        btns = proxy[len(self.array) - 1]()
        return KeyboardMessage(
            tracking_data='tracking_data', keyboard={
                "Type": "keyboard",
                "Buttons": btns,
            }
        )

    def create_buttons_1(self):
        return [
            self.create_key(self.array[0], 6, 1),
        ]

    def create_buttons_2(self):
        return [
           self.create_key(self.array[0], 3, 1),
           self.create_key(self.array[1], 3, 1),
        ]

    def create_buttons_3(self):
        return [
            self.create_key(self.array[0], 3, 1),
            self.create_key(self.array[1], 3, 1),
            self.create_key(self.array[2], 6, 2),
        ]


def create_keyboard_by_texts_viber(array_texts):
    return CreateButtons(array_texts).create_buttons()


