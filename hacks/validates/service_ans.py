import json

from pydantic import BaseModel
import typing as t


class ResponseAppKeyboardsItem(BaseModel):
    text: str
    color: str = "primary"


class ResponseAppKeyboardsRow(BaseModel):
    buttons: t.List[ResponseAppKeyboardsItem]


class ResponseApp(BaseModel):
    text: str
    keyboards: t.List[ResponseAppKeyboardsRow] = None
    user_id: str

    def get_keyboards_vk(self):
        btns = dict(one_time=True, buttons=[])
        if self.keyboards is None:
            return json.dumps(btns)
        else:
            btns = dict(one_time=False, buttons=[])

        for row in self.keyboards:
            rows = []
            for item in row.buttons:
                rows.append(
                    {
                        "action": {
                            "type": "text",
                            "label": item.text
                        },
                        "color": item.color,
                    }
                )
            btns["buttons"].append(rows)
        return json.dumps(btns)

    def get_keyboards_telegram(self, user_telegram_keyboard):
        btns = []
        if self.keyboards is None:
            return None
        for row in self.keyboards:
            for item in row.buttons:
                btns.append(item.text)
        return user_telegram_keyboard(btns)


class RequestApp(BaseModel):
    platform: str
    user_id: str
    body: str
