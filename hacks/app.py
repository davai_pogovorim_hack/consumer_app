import fastapi_jsonrpc as jsonrpc
# Создаём приложение fast api и точку входа
app = jsonrpc.API()

api_v1 = jsonrpc.Entrypoint(
    "/jsonrpc",
    errors=jsonrpc.Entrypoint.default_errors,
)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run('hacks:app', host="0.0.0.0", port=8001, debug=True)
