#!/bin/bash

# Start server
echo "Starting server"
uvicorn hacks:app --host 0.0.0.0 --port=80
